; Syed Ahmed Hussain
; Class: BE(SE)-4B
; Reg.#: 05761
; Sub: Advanced Programming
; Lab 10: Scheme

; 1. Add function
(define ADD
  (lambda (a b)
    (succ a (succ b z))
  )
)

; 2. Subtract function
(define SUB
  (lambda (a b)
    (pred (succ b z) (succ a z) )
  )
)

; 3. And function
(define AND
  (lambda (M N)
    (N (M #t #f) #f)
  )
)

; 4. Or function
(define OR
  (lambda (M N)
    (N #t (M #t #f))
  )
)

; 5. Not function
(define NOT
  (lambda (M)
    (M #f #t)
  )
)

; 6. Less than or equal to function
(define LEQ
  (lambda (a b)
    (iszero (SUB a b) )
  )
)

; 7. Greater than or equal to function
(define GEQ
  (lambda (a b)
    (iszero (SUB b a) )
  )
)

; Other functions / variables

; variable 0
(define z 0)

; true function
(define true
  (lambda (a b)
    a
  )
)

; false function
(define false
  (lambda (a b)
    b
  )
)

; successor function
(define succ
  (lambda (a b)
    (+ a b)
  )
)

; predecessor function
(define pred
  (lambda (a b)
    (if (< (- b a) z)
       z
      (- b a)
    )
  )
)

; iszero function
(define iszero
  (lambda (a)
    (= a z)
  )
)

